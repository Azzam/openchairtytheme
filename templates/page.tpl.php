<header class="header">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <a href="<?php echo $front_page;?>">
                    <h1 class="site-name">
                        <span>Open Charity</span>
                    </h1>
                </a>
                <button class="btn-mobile btn-mobile--emphatic" type="button">
                    <span class="btn-mobile-box">
                      <span class="btn-mobile-inner"></span>
                    </span>
                </button>
                <nav class="m-mainmenu">
                <?php print render($page['main_menu'])?>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- endHeader -->


<!-- blockGet -->
<section class="l-main-section">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
              <div class="main-content">
                <?php print $messages; ?>
                <?php print render($title_prefix); ?>
                <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
                <?php print render($title_suffix); ?>
                <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
                <?php print render($page['help']); ?>
                <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
                <?php print render($page['content']); ?>
              </div>
            </div>
        </div>
    </div>
</section>
<!-- endblockGet -->


<!-- footer -->
<footer class="footer">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <?php print render($page['social_links'])?>
                <?php print render($page['footer'])?>
            </div>
        </div>
    </div>
</footer>
<!-- endfooter -->
