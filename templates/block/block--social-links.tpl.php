<?php //print $content ?>
<?php
$platforms = variable_get('social_media_links_platforms', array());
$links = social_media_links_platforms();
$fb = $links['facebook']['base url'].$platforms['facebook']['platform_value'];
$tw = $links['twitter']['base url'].$platforms['twitter']['platform_value'];
$gp = $links['googleplus']['base url'].$platforms['googleplus']['platform_value'];
?>
<ul class="footer__social">
    <li>
        <a href="<?php echo $fb; ?>" target="_blank">
            <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>
    </li>
    <li>
        <a href="<?php echo $tw; ?>" target="_blank">
            <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
    </li>
    <li>
        <a href="<?php echo $gp; ?>" target="_blank">
            <i class="fa fa-google-plus" aria-hidden="true"></i>
        </a>
    </li>
</ul>