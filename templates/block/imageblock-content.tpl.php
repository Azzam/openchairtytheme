<?php

/**
 * @file
 * Default theme implementation to display image block content.
 *
 * Available variables:
 * - $image: Block image.
 * - $content: Block content.
 * - $block: Block object.
 *
 * @see template_preprocess()
 * @see template_preprocess_imageblock_content()
 */

$style = '';
if ($image) {
	$doc = new DOMDocument();
	$doc->loadHTML($image);
	$xpath = new DOMXPath($doc);
	$src = $xpath->evaluate("string(//img/@src)");
	$style = 'style="background-image: url('.$src.');"';
}
?>

<div class="m-hero" <?php echo $style;?>>
	<div class="l-constrained">
		<div class="l-row">
			<div class="l-col grid_12">
			<?php print $content ?>
			</div>
		</div>
	</div>
</div>
