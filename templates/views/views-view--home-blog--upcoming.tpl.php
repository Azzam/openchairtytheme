<?php if ($rows){ ?>
  <?php //print $rows; ?>
<div class="l-col grid_10">
    <p class="m-heroregister__event-text">
        <a href="">Next Event:</a> 
        <!-- June 23 <sup>rd</sup> 2016 18:30 - 21:00 -->
        <?php echo $view->result[0]->field_event_calendar_date[0]['rendered']['#markup']?>
    </p>
    <p class="m-heroregister__text"><?php echo $view->result[0]->field_field_event_location[0]['rendered']['#markup']?></p>
</div>
<div class="l-col grid_2">
    <a href="<?php echo url('node/add/signup/'.$view->result[0]->nid);?>" class="m-btn btn-blue m-heroregister__btn">Register</a>
</div>
<?php }?>