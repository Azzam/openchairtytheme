<?php if ($rows){?>
  <?php $arr_rows = $view->style_plugin->rendered_fields; ?>
	<?php foreach($arr_rows as $key=>$arr_row){ $key++;?>
	<div class="l-col grid_4">
	    <div class="l-blocks-get__img0<?php echo $key?>">
	        <?php print $arr_row['field_image']; ?>
	    </div>
	    <h3><?php print $arr_row['title']; ?></h3>
	    <?php print $arr_row['body']; ?>
	    <?php print $arr_row['field_link']; ?>
	</div> 
	<?php }?>
<?php } ?>