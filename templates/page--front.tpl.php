<header class="header">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <a href="<?php echo $front_page;?>">
                    <h1 class="site-name">
                        <span>Open Charity</span>
                    </h1>
                </a>
                <button class="btn-mobile btn-mobile--emphatic" type="button">
                    <span class="btn-mobile-box">
                        <span class="btn-mobile-inner"></span>
                    </span>
                </button>
                <nav class="m-mainmenu"> <?php print render($page['main_menu'])?></nav>
            </div>
        </div>
    </div>
</header>
<!-- endHeader -->

<!-- Hero -->
<section class="m-herounit">

    <!-- Start Home Banner -->
    <?php print render($page['home_banner'])?>
    <!-- EOF Home banner -->  

    <div class="m-heroregister l-gray-bg">
        <div class="l-constrained">
            <div class="l-row">
            <?php print render($page['upcoming_event'])?>

            </div>
        </div>
    </div>
</section>
<!-- endHero -->

<!-- blockGet -->
<section class="l-blocks-get">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <h2>Get Involved</h2>
            </div>
            <?php print render($page['home_what_we_do'])?> 
        </div>
    </div>
</section>
<!-- endblockGet -->

<!-- blockMission -->
<section class="l-blocks-mission l-gray-bg" id="aoc">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <h2>Our mission</h2>
                <p class="l-blocks-mission__infotext">
                Charities and Partners collaborating and sharing open solutions and ideas to create value in the digital space..
                </p>
                <p>
                    <strong>If you are a charity or a supplier, we are ready to welcome you.</strong>
                </p>
            </div>
            <?php print render($page['home_missions'])?>  
        </div>
    </div>
</section>
<!-- endblockMission -->

<!-- blockMembers -->
<section class="l-blocks-member l-gray-bg">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <h2>Our Members</h2>
                <div class="m-carousel-members">
                <?php print render($page['home_members'])?>                  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- endblockMembers -->

<!-- blockBlog -->
<section class="l-blocks-blog">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <h2>Blog</h2>
                <div class="m-carousel-blog">
                    <!-- HOMEBLOG -->
                    <?php print render($page['home_blog'])?>
                    <!-- HOMEBLOG -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- endblockBlog -->

<!-- footer -->
<footer class="footer">
    <div class="l-constrained">
        <div class="l-row">
            <div class="l-col grid_12">
                <?php print render($page['social_links'])?>
                <?php print render($page['footer'])?>
            </div>
        </div>
    </div>
</footer>
<!-- endfooter -->