// Mobile Menu
$('.btn-mobile').click(function() {
  $(this).toggleClass('is-active');
  $('.m-mainmenu').slideToggle(500);
});

// fix menu on resize windows when its close!
$(window).resize(function() {
  if ($(window).width() > 768) {
    $('.m-mainmenu').show();
  } else {
    $('.m-mainmenu').hide();
  }
});

// equal height blocks
$('.block-bg').matchHeight();


// Member Carousel
$('.m-carousel-members').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 5,
  arrows: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});



// Blog Carousel

$('.m-carousel-blog').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

// scroll to
$('.m-mainmenu__scroll').on('click',function (e) {
	    e.preventDefault();
	    var target = $('#aoc');
	    var $target = $(target);
      var headerheight = $('.header').outerHeight();


	    $('html, body').stop().animate({
	        scrollTop: $target.offset().top-headerheight
	    }, 900, 'swing');
	});
