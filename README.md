# Drupal Theme Project

#### Author: Mohamed Azzam


## Installation


To install via npm:

```bash
$ npm install
$ bower install
```

To Run as app static HTML via localhost:

```bash
$ gulp serve
```

To Build Theme run as static files

```bash
$ gulp build
```

copy build folders to Drupal themes structure:

```bash
$ gulp copy
```
